const helpers = require('../common/helpers');
const PointElement = require('./../models/PointElement');
var http = require('http'), fs = require('fs');

class PointController {
    // POST /
    async create (req, res, param, postData) {
        postData = JSON.parse(postData);
        if(!this.validateData(postData)){
            return helpers.validationError(res);
        }
        PointElement.create(postData);
        return helpers.success(res);
    }
    // GET /points
    async getPoints (req, res) {
        return helpers.success(res, PointElement.getAll());
    }
    validateData(data) {
        return data.lat && data.lon && data.timestamp
    }
    async reset (req, res) {
        PointElement.deleteAll();
        return helpers.success(res);
    }
    index(req, res){
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
        fs.readFile('./views/index.html', null, function (error, data) {
            if (error) {
                res.writeHead(404);
                res.write('Whoops! File not found!');
            } else {
                res.write(data);
            }
            res.end();
        });
    }
}

module.exports = new PointController();