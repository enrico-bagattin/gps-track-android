const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

class PointElement {
    static create (data) {
        db.get('points')
            .push({ 
                lat: data.lat,
                lon: data.lon,
                date: data.timestamp,
                device: data.identifier,
                acc: data.acc,
                alt: data.alt,
                spd: data.spd
            })
            .write()
    }
    static getAll () {
        return db.get('points').value()
    }
    static deleteAll () {
        return db.get('points').remove().write()
    }
}

module.exports = PointElement;