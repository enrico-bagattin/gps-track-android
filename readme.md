# README
This server has been made for a repackaging lab test to save gps coordinates of the victim.

## Run server on port 3000
`node index.js`

## Deploy on https://tracking-lab.eu.openode.io/
`openode deploy`

## Test apis on Chrome console
### Post Points
```
fetch('http://localhost:3000', {
  method: 'POST',
  body: JSON.stringify({ lat: 51, lon: 71.33251, timestamp: '2020-10-09 15:11', identifier: '454110-df55f5df-df4dffd5fd' }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8'
  }
})
.then(res => res.json())
.then(console.log)
```

### Get Points
```
fetch('http://localhost:3000/points', {
  method: 'GET',
  headers: {
    'Content-type': 'application/json; charset=UTF-8'
  }
})
.then(res => res.json())
.then(console.log)
```

### Data example
```
{
  "points": [
    {
      "lat": 45.262328,
      "lon": 12.195484,
      "date": "2020-10-09 15:11",
      "device": "454110-df55f5df-df4dffd5fd"
    },
    {
      "lat": 45.274068,
      "lon": 9.093420,
      "date": "2020-10-10 18:05",
      "device": "454110-df55f5df-df4dffd5fd"
    }
  ]
}
```