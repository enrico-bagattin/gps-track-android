/**
 * We define all our routes in this file. Routes are matched using `path`.
 * 1. If "path" is a string, then we simply match with url
 * 2. If "path is a object, then we assume it is a RegEx and use RegEx matching
 */

const PointController = require('./controllers/PointController');

const routes = [
    // {
    //     method: 'GET',
    //     path: '/model',
    //     handler: modelController.index.bind(modelController)
    // },
    // {
    //     method: 'GET',
    //     path: /\/model\/([0-9a-z]+)/,
    //     handler: modelController.show.bind(modelController)
    // },
    {
        method: 'GET',
        path: '/',
        handler: PointController.index.bind(PointController)
    },
    {
        method: 'GET',
        path: '/points',
        handler: PointController.getPoints.bind(PointController)
    },
    {
        method: 'POST',
        path: '/',
        handler: PointController.create.bind(PointController)
    },
    {
        method: 'DELETE',
        path: '/',
        handler: PointController.reset.bind(PointController)
    }
];

module.exports = routes;