const http = require('http');
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
adapter = new FileSync('db.json')
const db = low(adapter)
// Set some defaults
db.defaults({ points: [] }).write()

const routes = require('./routes');
const router = require('./router');

process.on('uncaughtException', function(err) {
    // handle the error safely
    console.log('uncaughtException');
    console.error(err.stack);
    console.log(err);
});


const server = http.createServer(async (req, res) => {
    await router(req, res, routes);
});

server.listen(process.env.PORT || 3000, () => {
    console.log('Server is listening on port 3000');
});